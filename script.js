function fizzBuzz(maxValue) {
    let results = " ";
    for (let i=1; i<=maxValue; i++){
        if (i % 2 === 0 && i % 3 === 0) {
            results = results + "FizzBuzz, ";
        }  
        else if (i % 2 === 0) {
           results = results + "Fizz, ";
        }
        else if (i % 3 === 0) {
            results = results + "Buzz, ";
        } 
        else if (i % 2 !== 0 && i % 3 !== 0) {
            results = results + i + ", ";
        }
    }
    console.log(results)
}
fizzBuzz(12);

// function fizzBuzzPrime(maxValue){
//     let results = " ";
//     for (let i=1; i<=maxValue; i++){
//         if (i % 2 === 0 && i % 3 === 0) {
//             results = results + i+ "FizzBuzz, ";
//         }  
//         else if (i % 2 === 0) {
//            results = results +i+ "Fizz, ";
//         }
//         else if (i % 3 === 0) {
//             results = results +i+ "Buzz, ";
//         } 
//         else if (i % 2 !== 0 && i % 3 !== 0) {
//             results = results + i + ", ";
//         }
//     }
//     console.log(results)
// }
// fizzBuzz(12);